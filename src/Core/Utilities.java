package Core;

import java.security.MessageDigest;
import java.security.SecureRandom;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Utilities {
	//Utilities:
	static final String HEXES = "0123456789abcdef";
	public static String getHex( byte [] raw ) {
	    if ( raw == null ) {
	        return null;
	    }
	    final StringBuilder hex = new StringBuilder( 2 * raw.length );
	    for ( final byte b : raw ) {
	        hex.append(HEXES.charAt((b & 0xF0) >> 4))
	            .append(HEXES.charAt((b & 0x0F)));
	    }
	    return hex.toString();
	}
	
	public static String GenerateGUID(){
		SecureRandom sr = new SecureRandom();
		byte[] rndBytes = new byte[32];
		sr.nextBytes(rndBytes);
		String guid = getHex(rndBytes);
		Log.i("TAG", guid);
		return guid;
	}
	
	public static String betToKey(String choice) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        double ran = Math.random()*10;        		
	        byte[] hash = digest.digest(Double.toString(ran).getBytes());
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	        choice += hexString.substring(1);
	        return choice;
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
	String HexToBinary(String Hex) {
	    int i = Integer.parseInt(Hex, 16);
	    String Bin = Integer.toBinaryString(i);
	    return Bin;
	}
	
	private static int getInt_of_HexByte(byte b){

		if(b >= 48 && b <= 57){
			 return b - 48;
		}
		if(b >= 97 && b <= 102){
			return b - 97 + 10;
		}
		if(b>= 65 && b <= 70){
			return b - 65 + 10;
		}
		return -1;
	}
	public static String keyToHash(String key){
		try{

			byte[] tmp = key.getBytes();
			int l = tmp.length;
			byte[] binary_key = new byte[l/2];
			if(l % 2 != 0) l = l -1;
			for(int i = 0; i < l; i = i+2){
				byte b1 = tmp[i];
				byte b2 = tmp[i + 1];
				int i1 = Utilities.getInt_of_HexByte(b1);
				int i2 = Utilities.getInt_of_HexByte(b2);
				if(i1 != -1 && i2 != -1) {
					byte bin = (byte)(i1 * 16 + i2);
					binary_key[i/2] = bin;			
				}
			}
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");      
	        byte[] hash = digest.digest(binary_key);
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString();
	    } catch(Exception ex){
	    	Log.v("GAME", ex.getMessage());
	       throw new RuntimeException(ex);
	    }
	}

	public static boolean haveNetworkConnection(Context con) {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;

	    ConnectivityManager cm = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    return haveConnectedWifi || haveConnectedMobile;
	}
}


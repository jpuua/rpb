package DataClasses;

import org.json.JSONObject;

import android.util.Log;

public class Session {
	private int nr, test_bal, btc_bal, requests, test_unconfirmed, btc_unconfirmed, test_locked, btc_locked;
	
	public Session(JSONObject input){
		try {
			this.nr = Integer.parseInt(input.getString("nr"));
			this.test_bal = Integer.parseInt(input.getString("test_bal"));
			this.btc_bal = Integer.parseInt(input.getString("btc_bal"));
			this.requests = Integer.parseInt(input.getString("requests"));
			this.test_unconfirmed = Integer.parseInt(input.getString("test_unconfirmed"));
			this.btc_unconfirmed = Integer.parseInt(input.getString("btc_unconfirmed"));
			this.test_locked = Integer.parseInt(input.getString("test_locked"));
			this.btc_locked = Integer.parseInt(input.getString("btc_locked"));
		} catch (Exception e) {
			Log.v("Session_exception",e.getMessage());
		}
	}
	
	public int getTestBalance_Combined(){
		return this.getConfirmedTestBalance() + this.getUnconfirmedTestBalance();
	}
	
	public int getBTCBalance_Combined(){
		return this.getConfirmedBTCBalance() + this.getUnconfirmedBTCBalance();
	}
	
	public int getConfirmedTestBalance(){
		return this.test_bal;
	}
	
	public int getUnconfirmedTestBalance(){
		return this.test_unconfirmed;
	}
	
	public int getConfirmedBTCBalance(){
		return this.btc_bal;
	}
	
	public int getUnconfirmedBTCBalance(){
		return this.btc_unconfirmed;
	}
	
	public String toString(){
		return "Session_nr:" + this.nr + "\n" +
				"Test_bal:" + this.test_bal+ "\n" +
				"BTC_bal:" + this.btc_bal + "\n" +
				"Requests:" + this.requests + "\n";
	}
}

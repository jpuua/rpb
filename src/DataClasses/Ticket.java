package DataClasses;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

public class Ticket {
	public String nr, live, modula, session_nr, amount;
	public static final String BTC_ADDR = "1Erich1YUdkUAp9ynf4Rfw2ug8nBtuUmMu";
	public static final int NANOCOINS_PER_COIN = 100000000;
	public Date expiration, current_time;
	
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); 
	
	public Ticket(JSONObject ticket){
		try {
			this.nr = ticket.getString("nr").toString();
			if(!ticket.getString("expiration").equals("null")){
				this.expiration = df.parse(ticket.getString("expiration"));
			}
			if(!ticket.getString("current_time").equals("null")){
				this.current_time = df.parse(ticket.getString("current_time"));
			}
			this.live = ticket.getString("live").toString();
			this.modula = ticket.getString("modula").toString();
			this.session_nr = ticket.getString("session_nr").toString();
			this.amount = ticket.getString("amount").toString();
			
		}catch (Exception e) {
			Log.v("GAME", "Ticket_full_constructor");
			e.printStackTrace();
		}
		
	}
	
	public int getSatoshiAmount(){
		return Integer.parseInt(this.amount);
	}
	
	@SuppressLint("DefaultLocale")
	public String getBTCAmount(){
		return String.format("%d.%08d", this.getSatoshiAmount() / Ticket.NANOCOINS_PER_COIN, this.getSatoshiAmount() % Ticket.NANOCOINS_PER_COIN);
	}
	
	public boolean isLive(){
		switch(Integer.parseInt(this.live)){
			case(1):
				return true;
			default:
				return false;
		}
	}
}

package com.example.rpbdesign;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Game;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	SoundPool sp;
	int sfx_gold_1  = 0;
	int sfx_gold_2  = 0;
	int sfx_gold_3  = 0;
	int sfx_thump_1 = 0;
	int sfx_notice  = 0;
	MediaPlayer mediaPlayer = null;
	Game game = null;
	int ICON_NON =-1;
	int ICON_RCK = 0;
	int ICON_PPR = 1;
	int ICON_BTC = 2;
	int selected =-1;
	boolean icon_pressed = false;	
	
	int tick = 0;
	String btc_addr = "";
	String popup_message = "";
	String last_message = "";
	int    last_message_age = 0;
	boolean btc_addr_ok = false;
	boolean play_sfx_gold = false;
		
	int pot_balance = 0;
	int stake = 0;
	
	public void setStake(int stake){
		this.stake = stake;
		Log.v("GAME", "stake: " + Integer.toString(stake));
		this.send_message(String.format("R %1$s has ended. Your prem stake %2$s.", round_nr, stake));
		if(stake > 0) playSound(sfx_notice);
	}
	
	int btc_balance = 0;
	int playmoney_balance = 0;
	int round_nr = 0;
	int key_time = -1;
	
	boolean round_started  = false;
	boolean key_time_met  = false;

	AlertDialog.Builder alert_get_btc_addr;
	AlertDialog.Builder alert_message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final Handler handler = new Handler();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SharedPreferences settings = getPreferences(MODE_PRIVATE);
		String guid = settings.getString("GUID", "");
		this.game = Game.getInstance(guid, this);
				
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		sfx_gold_1  = sp.load(this, R.raw.gold_1, 1);
		sfx_gold_2  = sp.load(this, R.raw.gold_2, 1);
		sfx_gold_3  = sp.load(this, R.raw.gold_3, 1);
		sfx_thump_1 = sp.load(this, R.raw.thump_1, 1);
		sfx_notice  = sp.load(this, R.raw.notice, 1);
		
		final ImageButton btn_rock     = (ImageButton) findViewById(R.id.btn_rock);
		final ImageButton btn_paper    = (ImageButton) findViewById(R.id.btn_paper);
		final ImageButton btn_bitcoin  = (ImageButton) findViewById(R.id.btn_bitcoin);
		final ImageButton btn_playmoney= (ImageButton) findViewById(R.id.btn_playmoney);
		final Button      btn_depo_btc = (Button)      findViewById(R.id.btn_deposit_bitcoins);
		final Button      btn_depo_test= (Button)      findViewById(R.id.btn_deposit_playmoney);
		final Button      btn_next_round=(Button)      findViewById(R.id.btn_next_round);
		
		btn_rock.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    ImageButton aButton = (ImageButton)v;
		        
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
			    	if (selected == ICON_NON)
			    	aButton.setImageResource(R.drawable.btn_rock_pressed);   
			    	icon_pressed = true;
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {        	    	
			    	aButton.setImageResource(R.drawable.rock);        	    	        	    	        	    	        	    	
			    	if (select_bet(ICON_RCK)) playSound(sfx_thump_1);        	    	
			    	icon_pressed = false;
			    	redraw();
		        }
			    return true;
		    }
		});        
		
		btn_paper.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    ImageButton aButton = (ImageButton)v;
		        
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
			    	if (selected == ICON_NON)
			    	aButton.setImageResource(R.drawable.btn_paper_pressed); 
			    	icon_pressed = true;
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	aButton.setImageResource(R.drawable.paper);
			    	if (select_bet(ICON_PPR)) playSound(sfx_thump_1);    
			    	icon_pressed = false;
			    	redraw();
		        }
			    return true;
		    }
		});    
		
		btn_bitcoin.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    ImageButton aButton = (ImageButton)v;
		        
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
			    	if (selected == ICON_NON)
			    	aButton.setImageResource(R.drawable.btn_bitcoin_pressed);
			    	icon_pressed = true;
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	aButton.setImageResource(R.drawable.bitcoin);
			    	if (select_bet(ICON_BTC)) playSound(sfx_thump_1);    
			    	icon_pressed = false;
			    	redraw();
		        }
			    return true;
		    }
		});            
		
		btn_playmoney.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    ImageButton aButton = (ImageButton)v;
		        
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
			    	aButton.setImageResource(R.drawable.btn_playmoney_pressed);
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	//playSound(sfx_thump_1);
			    	if (game.UsePlayMoney()) {
			    		game.setUseBitcoins();
			    	    aButton.setImageResource(R.drawable.btn_playmoney_disabled);
			    	    send_message("Now playing on bitcoins.");
			    	}	
			    	else {
			    		game.setUsePlayMoney();		    		
			    		aButton.setImageResource(R.drawable.btn_playmoney_active);
			    		send_message("Now playing on playmoney.");
			    	}
		        }
			    return true;
		    }
		});        		
		
		btn_depo_btc.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {        	           	       
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	depo_credits("1");
			    	playSound(sfx_thump_1);
		        }
			    return false;
		    }
		});              
		
		btn_depo_test.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {        	           	       
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);        	    	   
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	depo_credits("0");
			    	playSound(sfx_thump_1);
		        }
			    return false;
		    }
		});         
		
		btn_next_round.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {        	           	       
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);        	    	   
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	if(game == null) return false;
			    	playSound(sfx_thump_1);
			    	game.resetRound();
			    	
			    	pot_balance  = 0;
			    	stake = 0;
			    	
			    	key_time = -1;
			    	key_time_met = false;
			    	game.end_time_met = false;
			    	round_started = false;
			    	
			    	selected = ICON_NON;
			    	game.client_calc = false;
			    	game.GetNextRound();
			    	//TODO:
			    	//Cooldown
		        }
			    return false;
		    }
		});           
		
		final TextView addr = (TextView) findViewById(R.id.txt_btc_addr);
		addr.setText("ENTER BITCOIN ADDRESS HERE");
		
		addr.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    TextView txt = (TextView)v;
		        
			    if(event.getAction() == MotionEvent.ACTION_DOWN) {
			    	playSound(sfx_thump_1);
			    	txt.setTextColor(0xFFFF0000);    
			    	addr.setPaintFlags(addr.getPaintFlags()^ Paint.UNDERLINE_TEXT_FLAG);
		        } 
			    else if (event.getAction() == MotionEvent.ACTION_UP) {
			    	playSound(sfx_thump_1);
			    	txt.setTextColor(0xFFFFFFFF);
			    	addr.setPaintFlags(addr.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
			    	alert_get_btc_addr.show();      
			    	prepare_alert_get_btc_addr();
			    }
			    return true;
		    }
		});
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
		    @Override
		    public void run() {
		        handler.post(new Runnable() {
		            public void run() {                        
		                step();
		                redraw();
		            }
		        });
		    }
		},1000,1000);
		
		prepare_alert_get_btc_addr();        
		SharedPreferences prefs = getPreferences(MODE_PRIVATE); 
		String restoredText = prefs.getString("btc_addr", null);
		if (restoredText != null) {
			btc_addr = restoredText;
			game.btc_addr = btc_addr;
			TextView baddr = addr;            
		    baddr.setText(btc_addr);
		    btc_addr_ok = true;
		}
		
		mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.dst_gateway);
		mediaPlayer.setLooping(true);
		if (mediaPlayer != null) mediaPlayer.start();
	}
	
	private void step() {
        tick++;
        last_message_age++;
        
        //Set active round_nr;
        if(this.game != null && this.game.getActiveRound() != null) this.round_nr = this.game.getActiveRound().getRoundNr();
        
        //Refresh data
        //Round data is by default refreshed in every 10 seconds
        if(tick % 10 == 0){
        	this.game.refreshRound();
        }
        //In every 30 seconds speculations and session is updated
        if(tick % 30 == 0){
        	this.game.populateSpeculations();
        	this.game.populateSession();
        }
        
        //If key_time is met then round has to be finalized
        if(key_time_met == true && 
        		game.isFinalized() == false && 
        		game.end_time_met == false
        		&& game.speculation_made == true){
        	Log.v("STEP", "key_time_met = "+Boolean.toString(key_time_met));
        	Log.v("STEP", "Finalized = " + Boolean.toString(game.isFinalized()));
        	Log.v("STEP", "Finalize_in_step");
        	this.game.finalizeBet();
        	this.game.populateSession();
        	this.game.populateSpeculations();
        }
        
        Button btn=(Button)findViewById(R.id.btn_deposit_bitcoins);
    	TextView addr = (TextView) findViewById(R.id.txt_btc_addr);
    	addr.setTextColor(0xFFFFFFFF);
    	btn.setEnabled(false);
    	if (btc_addr.length() == 0) {
            addr.setPaintFlags(addr.getPaintFlags()^ Paint.UNDERLINE_TEXT_FLAG);
            addr.setText("ENTER BITCOIN ADDRESS HERE");
        }
        else if (!btc_addr_ok) {                        	
        	if (tick % 2 == 0) addr.setTextColor(0xFFFFFFFF);
        	else               addr.setTextColor(0xFF00FFFF);
        }
        else if (btc_addr_ok) {                        	
        	btn.setEnabled(true);
    	}
        
    	if (play_sfx_gold) {
    		Random rand = new Random();
    		int sfx = rand.nextInt(3);
    		switch (sfx) {
    		    case  0: playSound(sfx_gold_1); break;
    		    case  1: playSound(sfx_gold_2); break;
    		    default: playSound(sfx_gold_3); break; 
    		}
    		play_sfx_gold = false;
    	}
    	
    	if (popup_message.length() > 0) {
    		prepare_alert_message(popup_message);
    		popup_message = "";
    		alert_message.show();
    	}
    	
    	if(game.getActiveRound() != null && round_started == false){
    		if(game.getActiveRound().getKeyTime() != null && game.getActiveRound().getEndTime() != null){
	    		key_time = this.timeDiff(game.getActiveRound().getCurrentTime(), game.getActiveRound().getKeyTime());
	    		Log.v("STEP", "Calculated key_time: " + Integer.toString(key_time));
	    		round_started = true;
    		}
    	}
    	
    	if(round_started){
	    	if (key_time > 0) key_time--;
	    	else              key_time_met = true;
    	}
    	
    	// TODO:
        // Set btc_addr_ok to TRUE when server has approved it.
        if (tick % 10 == 0 && !btc_addr_ok
        &&  btc_addr.length() > 0) {
        	btc_addr_ok = true;
        	popup_message = "`"+btc_addr+"` is now accepted by the server as your Bitcoin receiving address.";	
        
        }
        
        //Update pot_balance
        if(this.game.getActiveRound() != null){
        	//If pot balances are equal nothing is done
        	if(pot_balance != this.game.pot_balance){ 
        		if(pot_balance < this.game.pot_balance) play_sfx_gold = true;;	
        		pot_balance = this.game.pot_balance;
        	}
        	//Update playmoney_balance
        	if(playmoney_balance != this.game.playmoney_balance){
        		playmoney_balance = this.game.playmoney_balance; 
        	}
        	//Update btc_balance
        	if(btc_balance != this.game.btc_balance){
        		btc_balance = this.game.btc_balance; 
        	}
        }
    }
	
	//Calculate time difference in seconds
	private int timeDiff(Date curTime, Date futTime){
		int diff = (int) (futTime.getTime() - curTime.getTime());
		int diffSeconds = diff / 1000 % 60; 
		
		if(diffSeconds > 0) return diffSeconds;
		else return -1;
	}
	
	private void redraw() {
		TextView txt = (TextView)findViewById(R.id.txt_potSize);
		txt.setText(String.format("Pot:%10d\nStake:%8d", pot_balance, stake));
		
		txt = (TextView)findViewById(R.id.txt_btc_balance);
		txt.setText(String.format("Balance:%12d", btc_balance));
		
    	if (game.UseBitcoins())  txt.setTextColor(0xFFFFFFFF);
    	else               txt.setTextColor(0x99FFFFFF);
		
		txt = (TextView)findViewById(R.id.txt_test_balance);
		txt.setText(String.format("Playmoney:%10d", playmoney_balance));	

    	if (game.UsePlayMoney()) txt.setTextColor(0xFFFFFFFF);
    	else               txt.setTextColor(0x99FFFFFF);		

		txt = (TextView)findViewById(R.id.txt_last_message);
		txt.setText(String.format("%s", last_message));
		switch ( (last_message_age/2) ) {
		    case  0:  txt.setTextColor(0xFFFFFF00); break;
		    case  1:  txt.setTextColor(0xFFFFFF33); break;
		    case  2:  txt.setTextColor(0xEEFFFF66); break;
		    case  3:  txt.setTextColor(0xEEFFFF99); break;
		    case  4:  txt.setTextColor(0xDDFFFFCC); break;
		    case  5:  txt.setTextColor(0xDDFFFFFF); break;
		    default:  txt.setTextColor(0xCCFFFFFF); break;
		}
    	
		txt = (TextView)findViewById(R.id.txt_round_nr);
		txt.setText(String.format("%d", round_nr));		
		
		Button button = (Button)findViewById(R.id.btn_next_round);
		ImageButton btn_switch = (ImageButton)findViewById(R.id.btn_playmoney);
		btn_switch.setEnabled(true);
		button.setEnabled(false);
		if (key_time_met == true){//end_time_met == true) {
		    button.setText("NEXT");
		    button.setEnabled(true);
		}
		else{
			if(key_time == -1){
				button.setText("WAIT");

			}else{
				button.setText(String.format("%04d", key_time));
				btn_switch.setEnabled(false);
			}
		}
		
		if (!icon_pressed) {
			ImageButton btn = (ImageButton)findViewById(R.id.btn_bitcoin);
			     if (selected == ICON_BTC) btn.setImageResource(R.drawable.btn_bitcoin_active);
			else if (selected == ICON_NON) btn.setImageResource(R.drawable.bitcoin);
			else                           btn.setImageResource(R.drawable.btn_bitcoin_disabled);
			
			btn = (ImageButton)findViewById(R.id.btn_rock);
			     if (selected == ICON_RCK) btn.setImageResource(R.drawable.btn_rock_active);
			else if (selected == ICON_NON) btn.setImageResource(R.drawable.rock);
			else                           btn.setImageResource(R.drawable.btn_rock_disabled);			     

			btn = (ImageButton)findViewById(R.id.btn_paper);
			     if (selected == ICON_PPR) btn.setImageResource(R.drawable.btn_paper_active);
			else if (selected == ICON_NON) btn.setImageResource(R.drawable.paper);
			else                           btn.setImageResource(R.drawable.btn_paper_disabled);		
		}
	}
	
	private void prepare_alert_get_btc_addr() {
        alert_get_btc_addr = new AlertDialog.Builder(this);
        alert_get_btc_addr.setTitle("Account Setup");
        alert_get_btc_addr.setMessage("Enter your Bitcoin address:"); 
        
        final LinearLayout alert_layout = new LinearLayout(this);
        final EditText bitcoinAddress = new EditText(this);
        final ImageButton btn_openWallet = new ImageButton(this);
		
		alert_layout.setOrientation(1);
    	alert_layout.addView(bitcoinAddress);
		btn_openWallet.setImageResource(R.drawable.wallet_icon);
		btn_openWallet.setScaleType(ScaleType.FIT_CENTER);
		btn_openWallet.setAdjustViewBounds(true);
		btn_openWallet.setBackgroundResource(0);
		
		btn_openWallet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i;
				PackageManager manager = getPackageManager();
				try {
				    i = manager.getLaunchIntentForPackage("de.schildbach.wallet");
				    if (i == null)
				        throw new PackageManager.NameNotFoundException();
				    i.addCategory(Intent.CATEGORY_LAUNCHER);
				    startActivity(i);
				} catch (PackageManager.NameNotFoundException e) {
					//Wallet not installed
					
				}
			}
			
        });
		alert_layout.addView(btn_openWallet);
		alert_get_btc_addr.setView(alert_layout);
        alert_get_btc_addr.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	TextView addr = (TextView) findViewById(R.id.txt_btc_addr);
	        	String baddr = bitcoinAddress.getText().toString();
	            // TODO:
	        	// Validate the bitcoin address client-side.
	        	btc_addr = baddr;
	        	game.btc_addr = btc_addr;
	            btc_addr_ok = false;
	            addr.setText(btc_addr);
	            SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
	            editor.putString("btc_addr", baddr);
	            editor.commit();            
	        }
        });   		
	}
	
	private void prepare_alert_message(String str) {
		alert_message = new AlertDialog.Builder(this);
		TextView myMsg = new TextView(this);
		myMsg.setText(str);
		myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
		alert_message.setView(myMsg);
	}
	
	public void show_wallet_not_found(String addr, String amount){
		this.popup_message = String.format("Wallet not found! Use third party wallet for transaction. \n Address:\n%s \n Amount: %s (BTC)",  addr, amount);
	}
	
	@Override
	public void onPause() {
	    super.onPause();  // Always call the superclass method first
	    if (mediaPlayer != null) mediaPlayer.pause();
	    savePrefChanges();
	}	
	
	@Override
	public void onResume() {
	    super.onResume();  // Always call the superclass method first

       SharedPreferences settings = getPreferences(MODE_PRIVATE);
       String guid = settings.getString("GUID", "");
       if(this.game == null){
           this.game = Game.getInstance(guid, this);
       }else{
	       if(this.game.getActiveRound() != null){
	    	   this.game.GetNextRound();
	       }
       }
	   if (mediaPlayer != null) mediaPlayer.start();
	}	
	
	@Override
    protected void onStop(){
       super.onStop();
       savePrefChanges();
    }
	
	private void savePrefChanges()
	{
	      // We need an Editor object to make preference changes.
	      // All objects are from android.context.Context
	      SharedPreferences settings = getPreferences(MODE_PRIVATE);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString("GUID", game.getGuid());
	      // Commit the edits!
	      editor.commit();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void playSound(int sfx) {			
		if(sfx != 0) sp.play(sfx, 1, 1, 0,0, 1);
	}
	
	public void send_message(String txt) {
		last_message = txt;
		last_message_age = 0;
		playSound(sfx_notice);
	}
	
	private boolean select_bet(int icon) {
		boolean initial = false;
		boolean out_of_money = false;
		if(game.getActiveRound() == null) return false;
		int status = game.getActiveRound().getLive(); 
		switch (status){
			case 0:
				if(game.playmoney_balance < 100) {
					out_of_money = true;
				}
				break;
			case 1:
				if(game.btc_balance < 100) {
					out_of_money = true;
				}
				break;
		}
		if(out_of_money) return false;
		if (selected == ICON_NON) {
		    initial = true;		    
		}
		        	    	
		if (key_time_met) return false;
		
		if (selected == icon) {
			return false;
//			selected = ICON_NON;
//			// TODO: cancel bet
//			pot_balance-=100;
//			if (use_bitcoins)  btc_balance+=100;
//			if (use_playmoney) playmoney_balance+=100;
		}
	    else {
	    	if (initial) {
		    	if ((game.UseBitcoins() && this.game.btc_balance >= 100)
		    	||  (game.UsePlayMoney() && this.game.playmoney_balance >= 100)) {
		    	    selected = icon;
		    	    play_sfx_gold = true;
					this.game.pot_balance += 100;
					if (game.UseBitcoins())  this.game.btc_balance -= 100;
					if (game.UsePlayMoney()) this.game.playmoney_balance -= 100;
					game.MakeBet(Integer.toString(icon), false, game.spec_nr);
		    	}
		    	else return false;	    		
	    	}
	    	else {
	    		game.MakeBet(Integer.toString(icon), true, game.spec_nr);
	    		selected = icon;
	    	}
	    }
		return true;
	}
	
	private void depo_credits(String live){
		if(!btc_addr_ok ) return;
		game.btc_addr = btc_addr;
		game.GetBTCTicket("10000", live);
	}

}

